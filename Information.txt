Unit Information;

Interface

 type MassType = array[1..1000] of String[6];

 function GetTheNumber(data: MassType; i, j, k: Integer): Integer;
 procedure InputTheData(var data: MassType; amountOfTasks: Integer);
 function IsDataAreCorrect(data: MassType; number1, number2, i: Integer): Boolean;
 procedure DetermineTheSummaryTime(data: MassType; amountOfTasks: Integer; var amountOfHours, amountOfMinutes: Integer);
 function IsTheEmployeeWillHaveTimeToSolveAllTasks(amountOfHours, amountOfMinutes: Integer): Boolean;
 procedure FindTheMinimumTask(data: MassType; amountOfTasks: Integer; var minTask, minHours, minMinutes: Integer);
 procedure FindTheMaximumTask(data: MassType; amountOfTasks: Integer; var maxTask, maxHours, maxMinutes: Integer);
 function GetTheConvertedTime(minutes, hours: Integer): Real;

Implementation

 function GetTheNumber(data: MassType; i, j, k: Integer): Integer;
 var number: Integer;
 begin
     number := (ord(data[i, j]) - ord('0')) * 10 + (ord(data[i, k]) - ord('0'));
     GetTheNumber := number
 end;

 procedure InputTheData(var data: MassType; amountOfTasks: Integer);
 var i, number1, number2: Integer;
 begin
     i := 1;
     while (i <= amountOfTasks) do
     begin
         Write('Task', i, ': ');
         ReadLn(data[i]);
         number1 := GetTheNumber(data, i, 1, 2);
         number2 := GetTheNumber(data, i, 4, 5);
         if (IsDataAreCorrect(data, number1, number2, i) = False) then
         begin
             WriteLn('Error! Try again!');
             Continue
         end;
         Inc(i)
     end
 end;

 function IsDataAreCorrect(data: MassType; number1, number2, i: Integer): Boolean;
 var key: Boolean;
 begin
     key := True;
     if ((number1 > 99) or (number1 < 0) or (number2 > 99) or (number2 < 0) or (data[i, 3] <> 'h') or (data[i, 6] <> 'm')) then
     begin
         key := False
     end;
     IsDataAreCorrect := key
 end;

 procedure DetermineTheSummaryTime(data: MassType; amountOfTasks: Integer; var amountOfHours, amountOfMinutes: Integer);
 var i: Integer;
 begin
     amountOfHours := 0;
     amountOfMinutes := 0;
     for i := 1 to amountOfTasks do
     begin
         amountOfHours := amountOfHours + GetTheNumber(data, i, 1, 2);
         amountOfMinutes := amountOfMinutes + GetTheNumber(data, i, 4, 5);
         while (amountOfMinutes >= 60) do
         begin
             Inc(amountOfHours);
             amountOfMinutes := amountOfMinutes - 60
         end
     end
 end;

 function IsTheEmployeeWillHaveTimeToSolveAllTasks(amountOfHours, amountOfMinutes: Integer): Boolean;
 var key: Boolean;
 begin
     key := True;
     if (amountOfHours > 40) or ((amountOfHours = 40) and (amountOfMinutes > 0)) then
     begin
         key := False
     end;
     IsTheEmployeeWillHaveTimeToSolveAllTasks := key
 end;

 procedure FindTheMinimumTask(data: MassType; amountOfTasks: Integer; var minTask, minHours, minMinutes: Integer);
 var i: Integer;
 begin
     minTask := 1;
     minHours := GetTheNumber(data, 1, 1, 2);
     minMinutes := GetTheNumber(data, 1, 4, 5);
     for i := 2 to amountOfTasks do
     begin
         if (minHours > GetTheNumber(data, i, 1, 2)) then
         begin
             minTask := i;
             minHours := GetTheNumber(data, i, 1, 2);
             minMinutes := GetTheNumber(data, i, 4, 5)
         end
         else if (minHours = GetTheNumber(data, i, 1, 2)) then
         begin
             if (minMinutes > GetTheNumber(data, i, 4, 5)) then
             begin
                 minTask := i;
                 minHours := GetTheNumber(data, i, 1, 2);
                 minMinutes := GetTheNumber(data, i, 4, 5)
             end
         end
     end
 end;

 procedure FindTheMaximumTask(data: MassType; amountOfTasks: Integer; var maxTask, maxHours, maxMinutes: Integer);
 var i: Integer;
 begin
     maxTask := 1;
     maxHours := GetTheNumber(data, 1, 1, 2);
     maxMinutes := GetTheNumber(data, 1, 4, 5);
     for i := 2 to amountOfTasks do
     begin
         if (maxHours < GetTheNumber(data, i, 1, 2)) then
         begin
             maxTask := i;
             maxHours := GetTheNumber(data, i, 1, 2);
             maxMinutes := GetTheNumber(data, i, 4, 5)
         end
         else if (maxHours = GetTheNumber(data, i, 1, 2)) then
         begin
             if (maxMinutes < GetTheNumber(data, i, 4, 5)) then
             begin
                 maxTask := i;
                 maxHours := GetTheNumber(data, i, 1, 2);
                 maxMinutes := GetTheNumber(data, i, 4, 5)
             end
         end
     end
 end;

 function GetTheConvertedTime(minutes, hours: Integer): Real;
 begin
     GetTheConvertedTime := hours + minutes / 60
 end;

end.                          